//
//  CVViewController.swift
//  Cv
//
//  Created by Aleksandar jelicic on 2018-10-29.
//

import UIKit

class CVViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
         // Do any additional setup after loading the view.
        text.scrollRangeToVisible(NSMakeRange(0,0))
        
            pic.layer.cornerRadius = pic.frame.width/2
            pic.layer.masksToBounds = true;
    }
    
    
    @IBOutlet weak var pic: UIImageView!
    
    
    @IBAction func EVC(_ sender: Any) {
        self.performSegue(withIdentifier: "EVCsegue", sender: nil)
    }
    
    
    @IBAction func SVC(_ sender: Any) {
        self.performSegue(withIdentifier: "SVCsegue", sender: nil)
    }
    
    @IBOutlet weak var text: UITextView!
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
