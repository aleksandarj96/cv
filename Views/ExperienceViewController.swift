//
//  ExperienceViewController.swift
//  Cv
//
//  Created by Aleksandar jelicic on 2018-10-29.
//

import UIKit
var workExp = ["Manpower, Ljungby", "Hjortsberga café, Hjortsberga", "V-Tab, Norrahammar"]
var workDesc = ["Arbetat inom lager och industri med paketering, plockning samt hantering av stora maskiner på flera Postnordlager i Ljungby. Har erfarenhet inom alla skift, morgon, kväll och natt. Ett jobb som har gett mycket bra erfarenheter med att jobba i stressiga miljöer, ansvarstagande och att kunna vara flexibel med arbetstiderna.", "Arbetat som kassör och servitör i en stressig miljö på ett lantcafé i Hjortsberga. Att jobba på Hjortsberga café har krävt mångsidighet, stresstålighet och att kunna hoppa in och jobba extra där det behövs.", "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Nam liber te conscient to factor tum poen legum odioque civiuda."]
var pics: [UIImage] = [
    UIImage(named: "wh")!,
    UIImage(named: "coffe")!,
    UIImage(named: "tryckeri")!,
]
var myIndex = 0
var mySection = 0

var year = ["12.2015-08.2017", "06.2012-09.2015", "06.2018-current"]

var edu = ["Naturvetenskapsprogrammet", "Datateknik"]
var eduDesc = ["Naturvetenskapsprogrammet, Katedralskolan, Växjö.", "Datateknik: Mjukvaruutveckling och mobila plattformar, 180 hp, Jönköping University."]
var eduYears = ["2012-2015", "2017-current"]

var eduPics: [UIImage] = [
    UIImage(named: "gymnasie")!,
    UIImage(named: "data")!,
]

struct header {
    var name : String!
    var sectionObjects : [String]!
    var pic : [UIImage]!
}

var array = [header]()

var heightHeader = 43.5

class ExperienceViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array[section].sectionObjects.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "cell")
        cell.textLabel?.text = array[indexPath.section].sectionObjects[indexPath.row]
        
        cell.imageView?.image = array[indexPath.section].pic[indexPath.row]
        
        return(cell)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        myIndex = indexPath.row
        mySection = indexPath.section
        performSegue(withIdentifier: "EDVCsegue", sender: self)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return array.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat(heightHeader)
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return array[section].name
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        array = [header(name: "Work", sectionObjects: workExp, pic: pics),
                 header(name: "Education", sectionObjects: edu, pic: eduPics)]

    }
}
