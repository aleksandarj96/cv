//
//  ExperienceDetailViewController.swift
//  Cv
//
//  Created by Aleksandar jelicic on 2018-10-29.
//

import UIKit

class ExperienceDetailViewController: UIViewController {

    @IBOutlet weak var desc: UITextView!
    
    @IBOutlet weak var workpic: UIImageView!
    
    @IBOutlet weak var myl: UILabel!
  
    @IBOutlet weak var years: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        switch mySection {
        case 0:
            self.title = array[0].name
            myl.text = workExp[myIndex]
            workpic.image = pics[myIndex]
            years.text = year[myIndex]
            desc.text = workDesc[myIndex]
        default:
            self.title = array[1].name
            myl.text = edu[myIndex]
            workpic.image = eduPics[myIndex]
            years.text = eduYears[myIndex]
            desc.text = eduDesc[myIndex]
        }
       
    }
}
