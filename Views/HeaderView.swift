//
//  HeaderView.swift
//  Cv
//
//  Created by Aleksandar jelicic on 2018-11-03.
//

import UIKit

class HeaderView: UITableViewCell {

    @IBOutlet weak var headerLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
