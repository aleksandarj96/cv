//
//  SkillsViewController.swift
//  Cv
//
//  Created by Aleksandar jelicic on 2018-10-29.
//

import UIKit

class SkillsViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        UiviewAnimation.layer.cornerRadius = UiviewAnimation.frame.width/4
        UiviewAnimation.layer.masksToBounds = true;
        
        
        UIView.animate(withDuration: 2, delay: 0,
                       options: [.autoreverse, .repeat],
                       animations: {
                        self.UiviewAnimation.transform = CGAffineTransform(scaleX: 7, y: 7)
        },
                       completion: nil
            
        )
     
    }

    
 
    @IBOutlet weak var UiviewAnimation: UIView!
    
    
}
